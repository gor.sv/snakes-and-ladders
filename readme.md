# Coding task

The question is to implement the first feature of
the '[Snakes and Ladders](http://agilekatas.co.uk/katas/SnakesAndLadders-Kata)' game. The feature is called 'Moving your
token'.

# Implementation notes

The project uses Java 11 (please see `app/build.gradle` → `java.toolchain.languageVersion` variable).

An `int` type is used instead of a `byte` even in places when we are not expected big numbers: e.g., a number of Board
spaces (1—100), or a number of spaces returned by a die (1—6). It could seem redundant but there are several reasons for
that:

* As summarized in
  the [Table in the Java Virtual Machine Specification](https://docs.oracle.com/javase/specs/jvms/se11/html/jvms-2.html#jvms-2.11.1)
  , all integral arithmetic operations, like adding, dividing and others, are only available for the type `int` and the
  type `long`, and not for the smaller types. So, when we use types smaller than `int`, a conversion to `int` and then
  back to the smaller type is performed for every integral arithmetic operation.
* The Java Virtual Machine models stacks and object fields using offsets that are (in effect) multiples of a 32-bit
  primitive cell size. So, when we declare a local variable or object field as (say) a `byte`, the variable/field will
  be stored in a 32-bit cell, just like an `int`.

Thus, in our case using primitive types smaller than `int` in fact would not give us a performance boost but even cause
performance degradation.
