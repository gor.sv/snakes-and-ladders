package snakes.and.ladders

import snakes.and.ladders.component.Token
import spock.lang.Specification

class TokenCanMoveAcrossTheBoardSpecification extends Specification {

    def "The token is placed on square 1 at the start of the game"() {
        given: "the game is started"
        when: "the token is placed on the board"
        Token token = new Token()

        then: "the token is on square 1"
        token.square == 1
    }

    def "The token can be moved from square #startSquare to square #endSquare by #spaces spaces"() {
        given: "the token is on square #startSquare"
        Token token = new Token()
        token.square = startSquare

        when: "the token is moved #spaces spaces"
        token.move(spaces)

        then: "the token is on square #endSquare"
        token.square == endSquare

        where:
        startSquare | spaces || endSquare
        1           | 3      || 4
        74          | 6      || 80
    }

    def "The token can be moved multiple times. Start square: #startSquare, first move: #firstSpaces spaces, second move: #secondSpaces spaces, end square: #endSquare"() {
        given: "the token is on square #startSquare"
        Token token = new Token()
        token.square = startSquare

        when: "the token is moved #firstSpaces spaces"
        token.move(firstSpaces)

        and: "then it is moved #secondSpaces spaces"
        token.move(secondSpaces)

        then: "the token is on square #endSquare"
        token.square == endSquare

        where:
        startSquare | firstSpaces | secondSpaces || endSquare
        1           | 3           | 4            || 8
        81          | 6           | 5            || 92
    }
}
