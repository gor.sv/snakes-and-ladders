package snakes.and.ladders

import snakes.and.ladders.component.Token
import snakes.and.ladders.controller.Player
import snakes.and.ladders.service.Die
import spock.lang.Specification

class PlayerCanWinTheGameSpecification extends Specification {

    def "The player has won the game"() {
        given: "the token is on square 97"
        Die die = Stub()
        Token token = new Token()
        token.square = 97
        Player player = new Player(die, token)

        when: "the token is moved 3 spaces"
        boolean hasWon = player.moveToken(3)

        then: "the token is on square 100"
        token.square == 100

        and: "the player has won the game"
        hasWon
    }

    def "The player has not won the game"() {
        given: "the token is on square 97"
        Die die = Stub()
        Token token = new Token()
        token.square = 97
        Player player = new Player(die, token)

        when: "the token is moved 4 spaces"
        boolean hasWon = player.moveToken(4)

        then: "the token is on square 97"
        token.square == 97

        and: "the player has not won the game"
        !hasWon
    }
}
