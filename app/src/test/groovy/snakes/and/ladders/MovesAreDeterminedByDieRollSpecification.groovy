package snakes.and.ladders

import snakes.and.ladders.component.Token
import snakes.and.ladders.controller.Player
import snakes.and.ladders.service.Die
import spock.lang.Specification

class MovesAreDeterminedByDieRollSpecification extends Specification {

    def "The die roll must return the result. Attempt #attempt"() {
        given: "the game is started"
        Die die = new Die()

        when: "the player rolls a die"
        int result = die.roll()

        then: "the result #result should be between 1-6 inclusive"
        result > 0
        result < 7

        where:
        attempt << (1..10)
    }

    def "The movement is determined by a die roll when the die roll is #spaces"() {
        given: "the player rolls a #spaces"
        Die die = Stub()
        die.roll() >> spaces
        Token token = Mock()
        Player player = new Player(die, token)
        int spacesToMove = player.rollDie()

        when: "they move their token"
        player.moveToken(spacesToMove)

        then: "the token should move #spaces spaces"
        1 * token.move(spaces)

        where:
        spaces << (1..6)
    }
}
