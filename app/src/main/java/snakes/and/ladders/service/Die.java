package snakes.and.ladders.service;

import org.springframework.stereotype.Service;

import java.security.SecureRandom;

@Service
public class Die {

    private static final int EXCLUSIVE_UPPER_BOUND = 6;

    private final SecureRandom secureRandom;

    public Die() {
        secureRandom = new SecureRandom();
    }

    public int roll() {
        return secureRandom.nextInt(EXCLUSIVE_UPPER_BOUND) + 1;
    }
}
