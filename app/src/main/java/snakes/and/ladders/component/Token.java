package snakes.and.ladders.component;

import org.springframework.stereotype.Component;

import static snakes.and.ladders.controller.Player.FINISHING_SQUARE;

@Component
public class Token {

    private static final int STARTING_SQUARE = 1;

    private int square = STARTING_SQUARE;

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public int move(int spaces) {
        int endSquare = square + spaces;
        if (endSquare <= FINISHING_SQUARE) {
            square = endSquare;
        }
        return square;
    }
}
