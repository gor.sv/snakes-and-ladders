package snakes.and.ladders.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import snakes.and.ladders.component.Token;
import snakes.and.ladders.service.Die;

@RestController
@RequestMapping("/v1/player")
public class Player {

    public static final int FINISHING_SQUARE = 100;

    private final Die die;
    private final Token token;

    public Player(Die die, Token token) {
        this.die = die;
        this.token = token;
    }

    @GetMapping("/roll-die")
    public int rollDie() {
        return die.roll();
    }

    @GetMapping("/move-token")
    public boolean moveToken(int spaces) {
        return token.move(spaces) == FINISHING_SQUARE;
    }
}
